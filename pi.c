#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <assert.h>
//utilice para compilar: gcc -fopenmp trap.c -o trap -lm  lm para el math.h 

#ifdef _OPENMP
 #include <omp.h>
#else
 #define omp_get_thread_num() 0
 #define omp_get_num_threads() 1
 #define omp_get_max_threads() 1
#endif

double time_diff(struct timeval* x , struct timeval* y);

int main(int argc, char* argv[]) {
 
 int n,res,num_exp,i,k;
 struct timeval before,after;
 double accum,* avgs,sum,factor;
 int ithread,ethread,curr_thread_count,sz;
 
 printf("Ingresar el numero de terminos de la serie y el numero de experimentos\n");
 res = scanf("%d %d",&n,&num_exp);
 #ifdef _OPENMP
 printf("Ingresar la cantidad base de threads del experimento[>=2] y la cantidad final de threads del experimento\n");
 res = scanf("%d %d",&ithread,&ethread);
 assert(ithread > 1);
 #else
 ithread = ethread = 1;
 #endif
 assert((n > 0) && (num_exp > 0) && (ithread <= ethread));
 sz = ethread - ithread + 1;
 avgs = (double*)malloc(sz * sizeof(double));
 assert(avgs);
 for(curr_thread_count = ithread; curr_thread_count <= ethread;++curr_thread_count)
 {
  accum = 0.0;
  for(i = 0;i < num_exp;++i)
  {
   sum = 0.0;
   gettimeofday(&before,NULL);
   #pragma omp parallel for num_threads(curr_thread_count) reduction(+:sum) private(factor)
   for (k = 0; k < n; ++k) 
   {
    if (k % 2 == 0)
     factor = 1.0;
    else
     factor = -1.0;
    sum += factor/((double)((k<<1)+1));
   }
   gettimeofday(&after,NULL);
   accum += time_diff(&before,&after);
  }
  //printf("avg time using %d threads:=%.8f and result trap_method:%.14f\n",thread_count,(accum/(double)num_exp),global_result);
  avgs[curr_thread_count - ithread] = (accum/(double)num_exp);
 }
 for(i = 0;i < sz;++i)
 {
   printf("tiempo promedio usando %d threads =%.14f microsegs\n",(i + ithread),avgs[i]);
 }
 #ifdef _OPENMP
 printf("numero de cores:%d\n",omp_get_max_threads());
 #endif
 free(avgs);
 return 0;
}

double time_diff(struct timeval* x , struct timeval* y)
{
    double x_ms,y_ms;
     
    x_ms = (double)(x->tv_sec*1000000) + (double)(x->tv_usec);
    y_ms = (double)(y->tv_sec*1000000) + (double)(y->tv_usec);
     
    return (y_ms - x_ms);
}
