#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <inttypes.h>
#include <time.h>
#ifdef _OPENMP
 #include <omp.h>
#endif

int main (int argc, char *argv[]){
srand(time(0));
int64_t in_circle = 0,i,ntosses;
float x,y,dist,pi;
printf("Ingresar el numero de lanzamientos(8 digitos recomendados)\n");
scanf("%"SCNd64,&ntosses);
printf("Espere por favor...\n");
#pragma omp parallel for default(none) reduction(+:in_circle) private(i,x,y,dist) shared(ntosses) 
for(i = 0;i < ntosses;++i)
{
 x = (((float)rand()/(float)RAND_MAX) * 2.0f) - 1.0f;
 y = (((float)rand()/(float)RAND_MAX) * 2.0f) - 1.0f;
 dist = (x * x) + (y * y);
 if(dist <= 1.0f)
  ++in_circle;
}
pi = (float)(4.0 * ((double)in_circle/(double)ntosses));
printf("pi:= %f\n",pi);
return 0;
}
