#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#ifdef _OPENMP
 #include <omp.h>
#else
 #define omp_get_thread_num() 0
 #define omp_get_num_threads() 1
#endif

#define N 5000

int A[N],B[N];
/*Ambos countSorts con complejidad n^2 con diferencia en el coeficiente(menor en la version con openmp);mientras que
el qsort tiene complejidad n*log(n) */
void countSort01(int a[],int n);
void countSort02(int a[],int n,int* temp);

int main (int argc, char *argv[]) {
int i;
char a_ord = 1,b_ord = 1;
int* buff = (int*)malloc(N * sizeof(int));
assert(buff);
for(i = 0;i < N;++i)
 A[i] = B[i] = rand();
countSort01(A,N);
#pragma omp parallel
countSort02(B,N,buff);

for(i = 0;i < (N - 1);++i)
{
 if(a_ord && (A[i] > A[i + 1]))
  a_ord = 0;
 if(b_ord && (B[i] > B[i + 1]))
  b_ord = 0;
 if(!a_ord && !b_ord)
  break;
}
printf("A %s esta ordenado y B %s esta ordenado\n",(a_ord?"si":"no"),(b_ord?"si":"no"));
free(buff);
return 0;
}

void countSort01(int a[],int n)
{
 int i,j,count,* temp;
 temp = (int*)malloc(n * sizeof(int));
 #pragma omp parallel for default(none) shared(n,temp,a) private(i,j,count)
 for(i = 0;i < n;++i)
 {
   count = 0;
   for(j = 0;j < n;++j)
   {
     if((a[j] < a[i]) || (a[j] == a[i] && j < i))
      ++count;
   }
   temp[count] = a[i];
 }
 memcpy(a,temp,n * sizeof(int));
 free(temp); 
}
void countSort02(int a[],int n,int* temp)
{
 int i,j,count,ini,end;
 int div = n / omp_get_num_threads();
 int rem = n % omp_get_num_threads();
 i = omp_get_thread_num();
 ini = div * i + (i <= rem? i : rem);
 end = ini + div + (i >= rem? (-1): 0);
 for(i = ini;i <= end;++i)
 {
   count = 0;
   for(j = 0;j < n;++j)
   {
     if((a[j] < a[i]) || (a[j] == a[i] && j < i))
      ++count;
   }
   temp[count] = a[i];
 }
 count = end - ini + 1;
 #pragma omp barrier
 memcpy(a + ini,temp + ini,count * sizeof(int)); 
}