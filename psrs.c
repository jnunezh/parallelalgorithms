 #include <stdio.h>
 #include <stdlib.h>
 #include <pthread.h>
 #include <time.h>
 #include <string.h>
 #include <sys/sysinfo.h>
 #include <assert.h>
 #include <mpi.h>
 #include <unistd.h> 
 #define N 10240000      
 #define MAX_FSZ_MB 2048
 
 typedef int sorted_type;
 MPI_Datatype mpi_element_type;
 //10000 * 1024
 struct Config
 {
  sorted_type* vec_ptr_buff;
  long num_elems;
 };
 
 struct ProcessThreadingInfo
 {
  int num_threads;
  long* fragments_assigned_count;
  int stack_top_idx;
  int process_rank;
  long num_elems_process; 
 };
  
 char src_file[] = "master_storage_file.dat";
 int system_cores,child_thread_num,rank,size;
 long curr_process_elements;
 pthread_t* threads;
 sorted_type* threads_memory_pool,* help_buff,* samples,* pivots; 
 struct Config* sections_configs;
 struct ProcessThreadingInfo* proc_thread_info;
  
 int compare(const void* ptr_a, const void* ptr_b);
 void* sort_fragment(void* arg);
 int createFileAndFill(long num_elems,const char* path);
 int FileContentsSorted(long num_elems,const char* path);
 int isSorted(sorted_type* vec,long num_elems);
 void printVec(sorted_type* vec,long num_elems);
 void merge(struct Config* sections_configs,size_t nfragments,sorted_type* hbuff);
 void modified_bsearch(sorted_type* vec,sorted_type key,long length,long* idx_at);
 void initReqVector(MPI_Request* vec,size_t size); 
 void waitAllRequests(MPI_Request* vec,size_t size);
 void fillWithRandom(sorted_type* vec,long num_elems);

 int main(int argc, char *argv[])
 {
  ldiv_t division;
  mpi_element_type = MPI_INT;
  long elemens_per_thread,* new_sections_count;
  int child_thread_num,i,base_tag = 88,out,end_msg,num_slaves;
  MPI_Init(&argc,&argv);
  MPI_Comm_rank(MPI_COMM_WORLD,&rank);
  MPI_Comm_size(MPI_COMM_WORLD,&size);
  division = ldiv(N,(size - 1));
  num_slaves = size - 1;
  samples = (sorted_type*)malloc(num_slaves * num_slaves * sizeof(sorted_type));
  help_buff = (sorted_type*) malloc(sizeof(sorted_type) * (2 * (N/num_slaves)));
  if(rank != 0)
  {

   struct Config* other_sections;
   
   out = MPI_Recv(&curr_process_elements,1,MPI_LONG,0,85,MPI_COMM_WORLD,MPI_STATUS_IGNORE);
   if(out != MPI_SUCCESS)
    printf("error_receive %d from %d\n",out,rank);
   //printf("@%d,curr_elemens2proc;%ld\n",rank,curr_process_elements);
   system_cores = get_nprocs();
   assert(system_cores > 1);  
   child_thread_num = system_cores - 1; 
   threads = (pthread_t*) malloc(sizeof(pthread_t) * child_thread_num);
   sections_configs = (struct Config*) malloc(sizeof(struct Config) * child_thread_num);
   threads_memory_pool  = (sorted_type*) malloc(sizeof(sorted_type) * 2 * (N/num_slaves));
   division = ldiv(curr_process_elements,child_thread_num);
   elemens_per_thread = division.quot;
   
   long rem = division.rem;
   i = 0;
   for(i = 0;i < child_thread_num;++i)
    sections_configs[i].num_elems = elemens_per_thread;
   i = 0;
   while(rem > 0)
   {
    assert(i < child_thread_num);
    ++(sections_configs[i].num_elems);
     --rem;
    ++i;
    if(i == child_thread_num)
     i = 0;    
   }
   for(i = 0;i < child_thread_num;++i)
   {
    if(i > 0)
     sections_configs[i].vec_ptr_buff = sections_configs[i - 1].vec_ptr_buff + sections_configs[i - 1].num_elems;
    else
     sections_configs[i].vec_ptr_buff = threads_memory_pool; 
   }
   out = MPI_Send(&child_thread_num,1,MPI_INT,0,87,MPI_COMM_WORLD);
   if(out != MPI_SUCCESS)
    printf("error_send %d from %d\n",out,rank);
   
   for(i = 0;i < child_thread_num;++i)
   { 
    out = MPI_Recv(sections_configs[i].vec_ptr_buff,sections_configs[i].num_elems,mpi_element_type,0,88,MPI_COMM_WORLD,MPI_STATUS_IGNORE);
    if(out != MPI_SUCCESS)
     printf("error_rec %d from %d\n",out,rank);
    out = pthread_create(&(threads[i]),NULL,&sort_fragment,&(sections_configs[i]));
    if(out)
    {
     printf("thread creation err@%d\n",rank);  
     exit(0);
    }  
   }
   
   for(i = 0;i < child_thread_num;++i)
    pthread_join(threads[i],NULL);
   
   
   merge(sections_configs,child_thread_num,help_buff);
   
   
   for(i = 0;i < num_slaves;++i)
    samples[i] = threads_memory_pool[i * (N/(num_slaves * num_slaves))];
   
   out = MPI_Send(samples,num_slaves,mpi_element_type,0,90,MPI_COMM_WORLD);
   if(out != MPI_SUCCESS)
    printf("error_send %d from %d\n",out,rank);
   out = MPI_Recv(samples,num_slaves * num_slaves,mpi_element_type,0,91,MPI_COMM_WORLD,MPI_STATUS_IGNORE);
   if(out != MPI_SUCCESS)
    printf("error_receiv %d from %d\n",out,rank);
   free(sections_configs);
   sections_configs = (struct Config*) malloc(sizeof(struct Config) * num_slaves);
   other_sections = (struct Config*) malloc(sizeof(struct Config) * num_slaves);
   new_sections_count = (long*) malloc(num_slaves * sizeof(long));
   sorted_type value,* new_b;
   long idx_pivot,prev_idx = 0,len;
   
   int idx,idx_max_pivot_values = num_slaves - 1;
   
   //printVec(threads_memory_pool,curr_process_elements);
   //printf("-------------------------------------------------%d@\n",rank);

   sections_configs[0].vec_ptr_buff = threads_memory_pool; 
   for(i = 1;i <= idx_max_pivot_values ;++i)
   { 
     idx =  (i * num_slaves) + (num_slaves/2) - 1;
     value  = samples[idx];
     //printf("rank:%d,val:%d,",rank,value);
     modified_bsearch(threads_memory_pool,value,curr_process_elements,&idx_pivot);
     //printf("idx:%ld,rest:%ld\n",idx_pivot,(idx_pivot - prev_idx));
     sections_configs[i - 1].num_elems = idx_pivot - prev_idx;
     sections_configs[i].vec_ptr_buff = sections_configs[i - 1].vec_ptr_buff + sections_configs[i - 1].num_elems;
     prev_idx = idx_pivot;
   } 
   sections_configs[i - 1].num_elems = (curr_process_elements - prev_idx);

   size_t requests_len = 2 * (num_slaves - 1);
   MPI_Request* requests = (MPI_Request*) malloc(sizeof(MPI_Request) * requests_len);
   
   int j = 0,k;
   
   other_sections[0].vec_ptr_buff = help_buff;
   memcpy(help_buff,sections_configs[rank - 1].vec_ptr_buff,sections_configs[rank - 1].num_elems * sizeof(sorted_type));
   other_sections[0].num_elems = sections_configs[rank - 1].num_elems;
   new_b = help_buff + sections_configs[rank - 1].num_elems;
   initReqVector(requests,requests_len);
   for(i = 1;i < size;++i)
   {
     if(i != rank)
     {
       out = MPI_Isend(&(sections_configs[i - 1].num_elems),1,MPI_LONG,i,105,MPI_COMM_WORLD,&(requests[j]));
       if(out != MPI_SUCCESS)
         printf("error_mpi_isend %d from %d\n",out,rank); 
       out = MPI_Irecv(&(new_sections_count[i - 1]),1,MPI_LONG,i,105,MPI_COMM_WORLD,&(requests[j + 1]));  //rest_sections_count[i - 1] conteo en i
       if(out != MPI_SUCCESS)
         printf("error_receiv %d from %d\n",out,rank);
       j += 2;  
     }
     else
     {
       new_sections_count[i - 1] = -1;
     }  
   }
   waitAllRequests(requests,requests_len);
   
   initReqVector(requests,requests_len);
   j = 0;
   k = 1;
   
   
   for(i = 1;i < size;++i)
   {
      
     if(i != rank)
     {
       assert(k < num_slaves);
       assert((i - 1) < num_slaves);
       assert(j < requests_len);
       assert((j + 1) < requests_len); 
       other_sections[k].vec_ptr_buff = new_b;
       out = MPI_Isend(sections_configs[i - 1].vec_ptr_buff,sections_configs[i - 1].num_elems,mpi_element_type,i,106,MPI_COMM_WORLD,&(requests[j]));
       if(out != MPI_SUCCESS)
         printf("error_mpi_isend %d from %d\n",out,rank); 
       out = MPI_Irecv(new_b,new_sections_count[i - 1],mpi_element_type,i,106,MPI_COMM_WORLD,&(requests[j + 1]));  //rest_sections_count[i - 1] conteo en i
       if(out != MPI_SUCCESS)
         printf("error_receiv %d from %d\n",out,rank);
       j += 2;
       other_sections[k].num_elems = new_sections_count[i - 1];   
       new_b += new_sections_count[i - 1];
       ++k;   
     }  
   }
   waitAllRequests(requests,requests_len);
   long new_process_elements = 0;
   for(i = 0;i < num_slaves;++i)
   {
     new_process_elements += other_sections[i].num_elems;
   } 
   merge(other_sections,num_slaves,threads_memory_pool);
   
   //int sorted;
   //sorted = isSorted(threads_memory_pool,new_process_elements);
   //printf("sorted:%d@%d\n",sorted,rank); 
   out = MPI_Send(&new_process_elements,1,MPI_LONG,0,92,MPI_COMM_WORLD);
   if(out != MPI_SUCCESS)
    printf("error_send %d from %d\n",out,rank);
   out = MPI_Send(threads_memory_pool,new_process_elements,mpi_element_type,0,93,MPI_COMM_WORLD);
   if(out != MPI_SUCCESS)
    printf("error_send %d from %d\n",out,rank);
   
   
   free(threads);
   free(sections_configs);
   free(threads_memory_pool);
   free(other_sections);
   free(new_sections_count);
   free(requests);
   
  }
  else
  {
    FILE* fr,* fw;
    long count;
    ldiv_t odiv;
    int j = 0,t;
    proc_thread_info = (struct ProcessThreadingInfo*) malloc(sizeof(struct ProcessThreadingInfo) * num_slaves);
    long rem = division.rem;
    for(i = 0;i < num_slaves;++i)
     proc_thread_info[i].num_elems_process = division.quot;
    i = 0;
    while(rem > 0)
    {
     ++(proc_thread_info[i++].num_elems_process);
     --rem;
     if(i == num_slaves)
      i = 0;   
    }
    for(i = 0;i < num_slaves;++i)
    {
      out = MPI_Send(&(proc_thread_info[i].num_elems_process),1,MPI_LONG,i + 1,85,MPI_COMM_WORLD);
      if(out != MPI_SUCCESS)
       printf("error_send %d from %d\n",out,rank);
    }
    t = createFileAndFill(N,src_file);
    assert(t);
    j = 0;
    for(i = 1;i < size;++i)
    {
        out = MPI_Recv(&(proc_thread_info[j].num_threads),1,MPI_INT,i,87,MPI_COMM_WORLD,MPI_STATUS_IGNORE);
        if(out != MPI_SUCCESS)
         printf("error_rec %d from %d\n",out,rank);
       
        odiv = ldiv(proc_thread_info[j].num_elems_process,proc_thread_info[j].num_threads);
        proc_thread_info[j].fragments_assigned_count = (long*)malloc(proc_thread_info[j].num_threads * sizeof(long));
        proc_thread_info[j].stack_top_idx = 0;
        proc_thread_info[j].process_rank = i; 
        for(t = 0;t < proc_thread_info[j].num_threads;++t)
         proc_thread_info[j].fragments_assigned_count[t] = odiv.quot;
        rem = odiv.rem;
        t = 0; 
        while(rem > 0)
        {
         ++(proc_thread_info[j].fragments_assigned_count[t++]);
         --rem;
         if(t == proc_thread_info[j].num_threads)
          t = 0;  
        }      
        ++j;      
    }
    i = 0;
    
    fr = fopen(src_file,"rb");
    assert(fr);
    j = 0;
    curr_process_elements = 0; 
    while(curr_process_elements != N)
    {
      t = proc_thread_info[j].stack_top_idx;  
      if(t != proc_thread_info[j].num_threads) 
      { 
          count = proc_thread_info[j].fragments_assigned_count[t];
          fread(help_buff,sizeof(sorted_type),count,fr);
          out = MPI_Send(help_buff,count,mpi_element_type,proc_thread_info[j].process_rank,88,MPI_COMM_WORLD);
          if(out != MPI_SUCCESS)
            printf("error_send %d from %d\n",out,rank); 
          ++(proc_thread_info[j].stack_top_idx);
          curr_process_elements += proc_thread_info[j].fragments_assigned_count[t];
      }
      ++j;
      if(j == num_slaves)           
       j = 0;
    }
    fclose(fr);
    j = 0;
    
    for(i = 1;i < size;++i)
    {
      out = MPI_Recv(samples + (j * num_slaves),num_slaves,mpi_element_type,i,90,MPI_COMM_WORLD,MPI_STATUS_IGNORE);
      if(out != MPI_SUCCESS)
       printf("error_rec %d from %d\n",out,rank);
      ++j;       
    }
    qsort(samples,num_slaves * num_slaves,sizeof(sorted_type),compare);
    for(i = 1;i < size;++i)
    {
      out = MPI_Send(samples,num_slaves * num_slaves,mpi_element_type,i,91,MPI_COMM_WORLD);
      if(out != MPI_SUCCESS)
       printf("error_send %d from %d\n",out,rank);     
    }
    fw = fopen(src_file,"wb");
    count = 0;
    for(i = 1;i < size;++i)
    {
      out = MPI_Recv(&(proc_thread_info[i - 1].num_elems_process),1,MPI_LONG,i,92,MPI_COMM_WORLD,MPI_STATUS_IGNORE);
      if(out != MPI_SUCCESS)
       printf("error_rec %d from %d\n",out,rank);
      out = MPI_Recv(help_buff,proc_thread_info[i - 1].num_elems_process,mpi_element_type,i,93,MPI_COMM_WORLD,MPI_STATUS_IGNORE);
      if(out != MPI_SUCCESS)
       printf("error_rec %d from %d\n",out,rank);
      fwrite(help_buff,sizeof(sorted_type),proc_thread_info[i - 1].num_elems_process,fw);
      count += proc_thread_info[i - 1].num_elems_process;
    }
    fclose(fw); 
    printf("count at master:%ld\n",count);
    i = FileContentsSorted(N,src_file);
    printf("global file sorted:%d\n",i);
    
    for(i = 0;i < num_slaves;++i)
     free(proc_thread_info[i].fragments_assigned_count);
    free(proc_thread_info);   
  }
   
  free(samples);
  free(help_buff);
  
  MPI_Finalize();
  return 0;

 }

 void* sort_fragment(void* arg)
 {
   struct Config* conf = (struct Config*) arg;
   qsort(conf->vec_ptr_buff,conf->num_elems,sizeof(sorted_type),compare);
 }
 int isSorted(sorted_type* vec,long num_elems)
 {
  long i,k = num_elems - 1;
  for(i = 0;i < k;++i)
  {
   if(vec[i] > vec[i + 1])
    return 0;
  }
  return 1;
 }
 int FileContentsSorted(long num_elems,const char* path)
 {
   sorted_type buff[1024];
   int ok = 1;
   ldiv_t division = ldiv(num_elems,1024);
   long iterations = division.quot + (division.rem > 0? 1 : 0);
   long j,k,ub;
   int i;
   FILE* pFile = fopen(path,"rb");
   if(pFile)
   {
    for(j = 0;j < iterations && ok;++j)
    {
      if(j != (iterations - 1))
      { 
        fread(buff,sizeof(sorted_type),1024,pFile);
        ub = 1024 - 1; 
      }
      else
      {  
        if(division.rem > 0)
         k = division.rem; 
        else
         k = 1024; 
        fread(buff,sizeof(sorted_type),k,pFile);
        ub = k - 1;
      }
      for(k = 0; k < ub && ok;++k)
      {
       if(buff[k] > buff[k + 1])
        ok = 0;     
      }    
    }
    fclose(pFile);
   }
   else
    ok = 0;
   return ok;
 } 
 int createFileAndFill(long num_elems,const char* path)
 {
   sorted_type buff[1024];
   int ok = 1;
   ldiv_t division = ldiv(num_elems,1024);
   long iterations = division.quot + (division.rem > 0? 1 : 0);
   long j;
   int i;
   FILE* pFile = fopen(path,"wb");
   if(pFile)
   {
    for(j = 0;j < iterations;++j)
    {
      for(i = 0;i < 1024;++i)
       buff[i] = rand() % 800;
      if(j != (iterations - 1))
       fwrite(buff,sizeof(sorted_type),1024,pFile);
      else
       fwrite(buff,sizeof(sorted_type),(division.rem > 0? division.rem : 1024),pFile);  
    }
    fclose(pFile);
   }
   else
    ok = 0;
   return ok;
 }
 void printVec(sorted_type* vec,long num_elems)
 {
   long k;
   for(k = 0;k < num_elems;++k)
   {
    printf("%3d ",vec[k]);
    if((k + 1) % 20 == 0)
     printf("\n");
   }
 }
 void fillWithRandom(sorted_type* vec,long num_elems)
 {
   long k;
   for(k = 0;k < num_elems;++k)
    vec[k] = rand();
 }
 int compare(const void* ptr_a, const void* ptr_b)
 {
  return (*((int*)ptr_a) - *((int*)ptr_b));
 }
 void merge(struct Config* sections_configs,size_t nfragments,sorted_type* hbuff)
 {
   size_t i,p,q;
   int condit,condit2;
   sorted_type valA,valB;
   long k,sum_elems;
   int disA,disB;
   for(i = 1;i < nfragments;++i)
   {
     sum_elems = sections_configs[0].num_elems + sections_configs[i].num_elems;
     disA = 0,disB = 0;  
     for(k = 0,p = 0,q = 0;k < sum_elems;++k)
     {
       valA = sections_configs[i].vec_ptr_buff[p];
       valB = sections_configs[0].vec_ptr_buff[q];
       condit2 = (disA != disB);
       condit = (valA < valB);
       if((!condit2 && condit) || (condit2 && disB))
       {
         ++p; 
         hbuff[k] = valA; 
       }
       else
       {
         ++q;
         hbuff[k] = valB;   
       }   
          
       if(p == sections_configs[i].num_elems)
       {  
         disA = 1;
         p = 0;
       } 
       if(q == sections_configs[0].num_elems)
       {  
         disB = 1;
         q = 0; 
       }
     }
     sections_configs[0].num_elems = sum_elems;
     memcpy(sections_configs[0].vec_ptr_buff,hbuff,sizeof(sorted_type) * sum_elems);    
   }
 }
 void modified_bsearch(sorted_type* vec,sorted_type key,long length,long* idx_at)
 {
   long imin = 0,imax = length - 1,imid;
   char found = 0;
   while(imin < imax)
   {
     imid =  imin + (imax - imin)/2;
     if(vec[imid] == key)
     {
       found = 1;
       break;
     }
     else if(vec[imid] < key)
      imin = imid + 1;
     else
      imax = imid - 1;  
     
   }
   if(found)
    *idx_at = imid + 1;
   else
    *idx_at = imin;
 }
 void initReqVector(MPI_Request* vec,size_t size)
 {
   size_t i;
   for(i = 0;i < size;++i)
    vec[i] = MPI_REQUEST_NULL;
 } 
 void waitAllRequests(MPI_Request* vec,size_t size)
 {
   size_t i;
   MPI_Status status;
   for(i = 0;i < size;++i)
    MPI_Wait(&(vec[i]),&status);
 } 