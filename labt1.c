 #include <stdio.h>
 #include <stdlib.h>
 #include <pthread.h>
 #include <time.h>
 #include <string.h>
 #define N 100
 #define W 4
 
 struct WorkerInfo
 {
  int worker_id,row_start,row_end,num_rows;

 }; 

 void* fillSection(void* arg);
 void* multiplySection(void* arg);
 
 struct WorkerInfo exec_info[W];
 double A[N][N],B[N][N],C[N][N],D[N][N],E[N][N];
 
 void main()
 {
  
  pthread_t threads[W];
  int d = N / W;
  int r = N % W;
  int ini = 0;
  int i;
  for(i = 0;i < W;++i)
  { 
   exec_info[i].worker_id = i;
   exec_info[i].num_rows = d;
   if(r != 0)
    {
     ++(exec_info[i].num_rows);
     --r;
    }
    exec_info[i].row_start = ini;
    exec_info[i].row_end = ini + (exec_info[i].num_rows - 1);
    ini = exec_info[i].row_end + 1;  
   if(pthread_create(&threads[i],NULL,&fillSection,&(exec_info[i])))
    exit(0); 
  }
  for(i = 0;i < W;++i)
   pthread_join(threads[i],NULL);
  for(i = 0;i < W;++i)
  { 
   if(pthread_create(&threads[i],NULL,&multiplySection,&(exec_info[i])))
    exit(0); 
  }
  for(i = 0;i < W;++i)
   pthread_join(threads[i],NULL);  

  printf("%f\n",E[99][99]);
  exit(0);
 }

 

 void*  fillSection(void* arg)
 {
   struct WorkerInfo* info = (struct WorkerInfo*) arg;
   int i,j;
   for(i = info->row_start;i <= info->row_end ; ++i)
   {
     for(j = 0;j < N;++j)
     {
       A[i][j] = i + j;
       B[i][j] = i + 2 * j;
       C[i][j] = 2 * i + 3 * j;
       D[i][j] = 2 * i + j;
     }
   }
 }
 void*  multiplySection(void* arg)
 {
   struct WorkerInfo* info = (struct WorkerInfo*) arg;
   int i,j,k;
   for(i = info->row_start;i <= info->row_end ; ++i)
   {
     for(j = 0;j < N;++j)
     {
        E[i][j] = 0.0;
        for(k = 0;k < N;++k)
        {
          E[i][j] += (A[i][k] * B[k][j] + C[i][k] * D[k][j]);
        }
       
     }
   }
 }
 
 
