#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <sys/time.h>
#include <assert.h>
//utilice para compilar: gcc -fopenmp trap.c -o trap -lm  lm para el math.h

#ifdef _OPENMP
 #include <omp.h>
#else
 #define omp_get_thread_num() 0
 #define omp_get_num_threads() 1
 #define omp_get_max_threads() 1
#endif

double time_diff(struct timeval* x , struct timeval* y);
double currFunction(const double* arg);
void Trap(double a, double b, int n, double* global_result_p,double (*f)(const double*));

int main(int argc, char* argv[]) {
 
 double global_result;
 double a, b;
 int n,res,num_exp,i;
 struct timeval before,after;
 double accum,* avgs;
 int ithread,ethread,curr_thread_count,sz;
 
 printf("Ingresar a, b,numero de trapecios y el numero de experimentos\n");
 res = scanf("%lf %lf %d %d", &a, &b, &n,&num_exp);
 #ifdef _OPENMP
 printf("Ingresar la cantidad base de threads del experimento[>=2] y la cantidad final de threads del experimento\n");
 res = scanf("%d %d",&ithread,&ethread);
 assert(ithread > 1);
 #else
  ithread = ethread = 1;
 #endif
 assert(ithread <= ethread);
 assert((a < b) && (n > 0) && (num_exp > 0));
 sz = ethread - ithread + 1;
 avgs = (double*)malloc(sz * sizeof(double));
 assert(avgs);
 for(curr_thread_count = ithread; curr_thread_count <= ethread;++curr_thread_count)
 {
  accum = 0.0;
  for(i = 0;i < num_exp;++i)
  {
   global_result = 0.0;
   gettimeofday(&before,NULL);
   #pragma omp parallel num_threads(curr_thread_count)
   Trap(a, b, n, &global_result,&currFunction);
   gettimeofday(&after,NULL);
   accum += time_diff(&before,&after);
  }
  //printf("avg time using %d threads:=%.8f and result trap_method:%.14f\n",thread_count,(accum/(double)num_exp),global_result);
  avgs[curr_thread_count - ithread] = (accum/(double)num_exp);
 }
 for(i = 0;i < sz;++i)
 {
   printf("tiempo promedio usando %d threads =%.14f microsegs\n",(i + ithread),avgs[i]);
 }
 #ifdef _OPENMP
 printf("numero de cores:%d\n",omp_get_max_threads());
 #endif
 free(avgs);
 return 0;
}

double time_diff(struct timeval* x , struct timeval* y)
{
    double x_ms,y_ms;
     
    x_ms = (double)(x->tv_sec*1000000) + (double)(x->tv_usec);
    y_ms = (double)(y->tv_sec*1000000) + (double)(y->tv_usec);
     
    return (y_ms - x_ms);
}

double currFunction(const double* arg)
{
 return log(*arg);
}
void Trap(double a, double b, int n, double* global_result_p,double (*f)(const double*)) 
{
 double h, x, my_result;
 double local_a, local_b;
 int i, local_n;
 int my_rank = omp_get_thread_num();
 int thread_count = omp_get_num_threads();
 h = (b-a)/n;
 local_n = n/thread_count;
 local_a = a + my_rank*local_n*h;
 local_b = local_a + local_n*h;
 my_result = (f(&local_a) + f(&local_b))/2.0;
 for (i = 1; i <= local_n - 1; ++i) 
 {
  x = local_a + i*h;
  my_result += f(&x);
 }
 my_result = my_result*h;

#pragma omp critical
 (*global_result_p) += my_result;
 
 
}